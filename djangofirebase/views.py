import pyrebase
from django.shortcuts import render

from django import forms

config = {
    "apiKey": "AIzaSyCqXBDsEuluwguVASR4xjGXP5R3P7djGQU",
    "authDomain": "todolist-50890.firebaseapp.com",
    "databaseURL": "https://todolist-50890-default-rtdb.firebaseio.com",
    "projectId": "todolist-50890",
    "storageBucket": "todolist-50890.appspot.com",
    "messagingSenderId": "1037651930805",
    "appId": "1:1037651930805:web:97fdec7a79edfd358598fc",
    "measurementId": "G-SPD6LQY1JG"
}

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()
db = firebase.database()


def signIn(request):
    return render(request, "signIn.html")


def postSign(request):
    email = request.POST.get("email")
    passw = request.POST.get("pass")
    try:
        user = auth.sign_in_with_email_and_password(email, passw)
    except:
        message = "invalid credantials"
        return render(request, "signIn.html", {"msg": message})
    # saving user to session
    request.session["sess_user"] = user
    
    afficherMessage = getPetitMessage()

    return render(request, "welcome.html", {"e": email, "listeMessage": afficherMessage})


def postPetitMessage(request):
    data = {
        "petitMessage": request.POST.get("petitmessage")
    }

    # retrieve user from session
    user = request.session["sess_user"]

    # refresh token
    auth.refresh(user['refreshToken'])

    db.child("message").push(data, user["idToken"])

    afficherMessage = getPetitMessage()

    nom = "Pauline"

    return render(request, "welcome.html", {"e": user["email"], "listeMessage": afficherMessage, "moi": nom})


def getPetitMessage():

    listeMessage = {}
    messages = db.child("message").get()

    for m in messages.each():
        listeMessage[m.key()] = m.val()

    return listeMessage

    
