from django import forms

class Message

    class Meta:
        model = Post
        fields = ('title', 'text',)